﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyNotepad
{
    public partial class Form1 : Form
    {
        string path;
        string oldText;

        public Form1()
        {
            InitializeComponent();
            path = string.Empty;
            oldText = textBox1.Text;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("MyNotepad - version " + Application.ProductVersion
               + "\n\nCopyright © 2017 by @lvduyanh", "About");
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isnew = true;
            if (!oldText.Equals(textBox1.Text))
            {
                DialogResult dr = MessageBox.Show("Do you want to save changes?", "MyNotepad", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    save();
                }
                else if (dr == DialogResult.Cancel)
                {
                    isnew = false;
                }
            }
            if (isnew)
            {
                path = string.Empty;
                textBox1.Clear();
                oldText = textBox1.Text;
                statusBar1.Text = "Ready";
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool isOpen = true;
            if (!oldText.Equals(textBox1.Text))
            {
                DialogResult dr = MessageBox.Show("Do you want to save changes?", "MyNotepad", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    save();
                }
                else if (dr == DialogResult.Cancel)
                {
                    isOpen = false;
                }
            }
            if (isOpen)
            {
                using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "Text Documents (*.txt)|*.txt", ValidateNames = true, Multiselect = false })
                {
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            using (StreamReader sr = new StreamReader(ofd.FileName))
                            {
                                path = ofd.FileName;
                                statusBar1.Text = "Path: " + path;
                                Task<string> text = sr.ReadToEndAsync();
                                textBox1.Text = text.Result;
                                oldText = textBox1.Text;
                            }
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save();
        }

        private async void save()
        {
            bool success = true;
            if (string.IsNullOrEmpty(path))
            {
                using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Text Documents (*.txt)|*.txt", ValidateNames = true })
                {
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            using (StreamWriter sw = new StreamWriter(sfd.FileName))
                            {
                                await sw.WriteLineAsync(textBox1.Text);
                            }
                        }
                        catch (Exception exc)
                        {
                            success = false;
                            MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            if (success)
                            {
                                path = sfd.FileName;
                                statusBar1.Text = "Path: " + path;
                                oldText = textBox1.Text;
                            }
                        }
                    }
                }
            }
            else
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(path))
                    {
                        await sw.WriteLineAsync(textBox1.Text);
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    oldText = textBox1.Text;
                }
            }
        }

        private async void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool success = true;
            using (SaveFileDialog sfd = new SaveFileDialog() { Filter = "Text Documents (*.txt)|*.txt", ValidateNames = true })
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        using (StreamWriter sw = new StreamWriter(sfd.FileName))
                        {
                            await sw.WriteLineAsync(textBox1.Text);
                        }
                    }
                    catch (Exception exc)
                    {
                        success = false;
                        MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        if (success)
                        {
                            path = sfd.FileName;
                            statusBar1.Text = "Path: " + path;
                            oldText = textBox1.Text;
                        }
                    }
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (textBox1.CanUndo)
            {
                textBox1.Undo();
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox1.Paste();
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fd = new FontDialog();
            fd.Font = textBox1.Font;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Font = fd.Font;
            }
        }

        private void fontColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = textBox1.ForeColor;
            if (cd.ShowDialog() == DialogResult.OK)
            {
                textBox1.ForeColor = cd.Color;
            }
        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = textBox1.BackColor;
            if (cd.ShowDialog() == DialogResult.OK)
            {
                textBox1.BackColor = cd.Color;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            if (!oldText.Equals(textBox1.Text))
            {
                DialogResult dr = MessageBox.Show("Do you want to save changes?", "MyNotepad", MessageBoxButtons.YesNoCancel);
                if (dr == DialogResult.Yes)
                {
                    save();
                }
                else if (dr == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
